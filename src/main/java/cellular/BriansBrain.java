package cellular;

import java.util.Random;

import datastructure.CellGrid;
import datastructure.IGrid;

public class BriansBrain implements CellAutomaton {

    IGrid currentGen;

    public BriansBrain(int rows, int columns) {
        this.currentGen = new CellGrid(rows, columns, CellState.DEAD);
        initializeCells();
    }

    @Override
    public CellState getCellState(int row, int column) {
        return currentGen.get(row, column);
    }

    @Override
    public void initializeCells() {
		Random random = new Random();
		for (int row = 0; row < currentGen.numRows(); row++) {
			for (int col = 0; col < currentGen.numColumns(); col++) {
				if (random.nextBoolean()) {
					currentGen.set(row, col, CellState.ALIVE);
				} else {
					currentGen.set(row, col, CellState.DEAD);
				}
			}
		}
	}

    @Override
    public void step() {
		IGrid nextGen = currentGen.copy();
		for (int row = 0; row < numberOfRows(); row ++) {
			for (int col = 0; col < numberOfColumns(); col ++) {
				nextGen.set(row, col, getNextCell(row, col));
			}
		}
		currentGen = nextGen;
	}

    @Override
    public CellState getNextCell(int row, int col) {
        if (getCellState(row, col) == CellState.ALIVE) {
            return CellState.DYING;
        } else if (getCellState(row, col) == CellState.DEAD && countNeighbors(row, col, CellState.ALIVE) + countNeighbors(row, col, CellState.DYING) == 2) {
            // if the cell is dead and should come to life
            return CellState.ALIVE;
        } else {
            return CellState.DEAD;
        }
    }

    @Override
    public int numberOfRows() {
        return currentGen.numRows();
    }

    @Override
    public int numberOfColumns() {
        return currentGen.numColumns();
    }

    @Override
    public IGrid getGrid() {
        return currentGen;
    }

    // countNeighbors is copied from GameOfLife.java
    private int countNeighbors(int row, int col, CellState state) {
		if (row < 0 || col < 0 || row >= currentGen.numRows() || col >= currentGen.numColumns()) {
			throw new IndexOutOfBoundsException("Illegal grid index.");
		}
		int cells;
		if (currentGen.get(row, col) == state) {
			cells = -1;
		} else {
			cells = 0;
		}
		int rowsStart;
		int rowsEnd;
		int ColsStart;
		int ColsEnd;
		if (row == 0) { // if index is on the left edge
			rowsStart = row;
		} else {
			rowsStart = row -1;
		}
		if (row == currentGen.numRows() - 1) { // if index is on the right edge
			rowsEnd = row;
		} else {
			rowsEnd = row + 1;
		}
		if (col == 0) { // if index is on the upper edge
			ColsStart = col;
		} else {
			ColsStart = col -1;
		}
		if (col == currentGen.numColumns() - 1) { // if index is on the lower edge
			ColsEnd = col;
		} else {
			ColsEnd = col + 1;
		}
		for (int i = rowsStart; i <= rowsEnd; i++) {
			for (int j = ColsStart; j <= ColsEnd; j++) {
				if (currentGen.get(i, j) == state) {
					cells ++;
				}
			}
		}
		return cells;
	}

}
