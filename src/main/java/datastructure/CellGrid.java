package datastructure;

import cellular.CellState;

public class CellGrid implements IGrid {

    private int rows;
    private int cols;
    private CellState[][] grid;

    public CellGrid(int rows, int columns, CellState initialState) {
		if (rows <= 0) {
            throw new IllegalArgumentException("The grid can't have less than 1 rows, you tried to make one with " + rows + " rows.");
        }
        this.rows = rows;
        if (columns <= 0) {
            throw new IllegalArgumentException("The grid can't have less than 1 columns, you tried to make one with " + columns + " columns.");
        }
        this.cols = columns;
        this.grid = new CellState[rows][columns];
        for (int row = 0; row < rows; row++) {
            for (int col = 0; col < cols; col++) {
                grid[row][col] = initialState;
            }
        }
	}

    @Override
    public int numRows() {
        return rows;
    }

    @Override
    public int numColumns() {
        return cols;
    }

    @Override
    public void set(int row, int column, CellState element) {
        if (row < 0 || column < 0 || row >= rows || column >= cols) {
            throw new IndexOutOfBoundsException("Illegal grid index.");
        }
        grid[row][column] = element;
    }

    @Override
    public CellState get(int row, int column) {
        if (row < 0 || column < 0 || row >= rows || column >= cols) {
            throw new IndexOutOfBoundsException("Illegal grid index.");
        }
        return grid[row][column];
    }

    @Override
    public IGrid copy() {
        // :)
        IGrid clone = new CellGrid(rows, cols, CellState.DEAD);
        for (int row = 0; row < rows; row++) {
            for (int col = 0; col < cols; col++) {
                clone.set(row, col, get(row, col));
            }
        }
        return clone;
    }
    
}
